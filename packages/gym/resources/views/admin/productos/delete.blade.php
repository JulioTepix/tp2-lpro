@extends('adminlte::page')

@section('title', 'Productos')

@section('content_header')
    <h1>Productos</h1>
@stop

@section('content')
<div class="card">
{!! Form::open(['route' => 'productos.destroy', 'method' => 'post']) !!}
{!! Form::hidden('id_producto',$data->id_producto) !!}
    <div class="card-header" >
        <h1 class="card-title">Producto a eliminar:</h1>
    </div>
    <div class="card-body">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
            <fieldset><Legend>{{$data->nombre}}</Legend>
            <div class="row">
              </div>
                </div>
                <div class="row">
                
                <div class="col-lg-2 col-md-6 col-sm-6 col-xs-12">
                        {!! Form::UTImage('assets/uploads/'.$data->imagen, '300px', '300px')!!}
                    </div>
                    
                    
                </div>
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <H1>¿Seguro que desea eliminar este producto?</H1>
                    <a type="button" href="{{ route('productos.index')}}" class="btn btn-outline-dark">No</a>
                    <button data-cy="conf-delete" type="submit" href="" class="btn btn-outline-dark" title="Delete">Si</button>
                    </div>
                </div>
            </fieldset>
        </div>
    </div>
</div>
@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
    <script> console.log('Hi!'); </script>
@stop
