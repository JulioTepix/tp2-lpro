
@extends('adminlte::page')

@section('title', 'Productos')

@section('content_header')
    <h1>Productos</h1>
@stop

@section('content')
<div class="card">
        <div class="header">
        
            
        </div>
       
<!-- Hover Rows -->
<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            <div class="body table-responsive">

                <!-- Buscador  -->
                <div class="row">
                    <div class="col-md-7 col-sm-7 col-xs-12">
                        <h5><strong>{{$data->total()}}</strong> Registro(s) encontrado(s).
                            P&aacute;gina
                            <strong>{{($data->total()==0) ? '0' : $data->currentPage()}}</strong> de
                            <strong> {{$data->lastPage()}}</strong>.
                            Registros por p&aacute;gina
                            <strong>{{($data->total()==0) ? '0' : $data->perPage()}}</strong></h5>
                    </div>
                    <div class="title_right">
                        <div class="col-md-10 col-sm-8 col-xs-12 form-group pull-right top_search">

                            <form action="{{route('productos.index')}}" method="get">
                                <div class="input-group">
                                    <input data-cy="search-prod" type="text" class="form-control" name="search"
                                           value="{{$search}}"
                                           placeholder="Nombre/Marca" style="border: 1px solid #ccc!important;width: 98%;">
                                    <span class="input-group-btn">
                                                <button data-cy="search" class="btn btn-outline-dark" type="submit">Buscar!</button>
                                            </span>
                                </div>
                            </form>

                        </div>
                    </div>
                </div>
                <a data-cy="new-prod" style="margin: 0 0 10px 85%" type="button" href="{{ route('productos.create')}}" class="btn btn-outline-success">Nuevo registro</a>
        
                <!-- Finalizar buscador  -->
            <div class="row gx-4 gx-lg-5">
            @foreach($data as $row)
                <div class="col-md-4 mb-5">
                    <div class="card h-100">
                        <div class="card-body">
                            <h2 class="card-title text-center">{{$row->nombre}}</h2><br>
                            <img class="img-fluid rounded " src="/assets/uploads/{{$row->imagen}}" width="100" height="100"></img>
                            <p class="card-text">Marca: {{ $row->marca}}</p>
                            <p class="card-text">Descripción: {{ $row->descripcion}}</p>
                            <p class="card-text">Stock: {{ $row->stock}}</p>
                            <p class="card-text">Precio: {{ $row->precio}} €</p>
                        </div>
                        <div class="btn-group" style="display: inline!important; margin: 0 0 10px 10px">
                                    <a data-cy="edit-prod" class='btn btn-outline-primary' style="float: none!important;" href="{{route('productos.update', $row->id_producto)}}" title='Editar registro'><i class="material-icons">Editar</i></a>
                                    <a data-cy="delete-prod" class='btn btn-outline-danger' style="float: none!important;" href="{{route('productos.delete', $row->id_producto)}}" title='Eliminar registro'><i class="material-icons">Eliminar</i></a>

                        </div>
                    </div>
                </div> 
                @endforeach   
            </div>

                
              

            </div>
        </div>
    </div>
</div>
    </div>
@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
    <script> console.log('Hi!'); </script>
@stop
