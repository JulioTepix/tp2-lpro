
@extends('adminlte::page')

@section('title', 'Productos')

@section('content_header')
    <h1>Productos -> Nuevo</h1>
@stop

@section('content') 
    <div class="card">
        {!! Form::open(['route' => 'productos.store', 'enctype' => "multipart/form-data", 'file' => true, 'method' => 'post']) !!}
        <div class="card-header">
            <h1 class="card-title">Datos del producto</h1>
        </div>
        <div class="card-body">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <fieldset><legend>Datos generales:</legend>
                <div class="row">
                <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
                        {!! Form::UTText('prod-name','nombre', 'Nombre', 'Nombre del producto', null, $errors, 40, true) !!}
                    </div>
                    <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
                        {!! Form::UTText('prod-com', 'marca', 'Marca', 'Marca del producto', null, $errors, 40, true) !!}
                    </div>
                    <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
                        {!! Form::UTText('prod-des', 'descripcion', 'Descripción', 'Descripción del producto', null, $errors, 100, true) !!}
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                        {!! Form::UTNumeric('prod-stock','stock', 'Stock', 'Cantidad disponible del producto', null, $errors, 4, true) !!}
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                        {!! Form::UTNumeric('prod-price','precio', 'Precio', 'Precio del producto', null, $errors, 10, true) !!}
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                        {!! Form::UTFile('imagen', 'Imagen', 'imagen del producto', true) !!}
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <button data-cy="submit" type="submit" href="" class="btn btn-outline-info" title="Guardar">Guardar</button>
                    </div>
                </div>
                </fieldset>
            </div>
        </div>
        {!! Form::close() !!}
    </div>

@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
    <script src="{{URL::asset('js/validatorFields.js')}}"></script>
@stop
