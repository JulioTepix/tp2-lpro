
@extends('adminlte::page')

@section('title', 'Productos')

@section('content_header')
    <h1>Productos -> Editar producto</h1>
@stop

@section('content') 
    <div class="card">
        {!! Form::model($data,['route' => ['productos.edit', $data->id_producto], 'enctype' => "multipart/form-data", 'file' => true, 'method' => 'put']) !!}
        {!! Form::hidden('id_producto', $data->id_producto)!!}
        <div class="card-header">
            <h1 class="card-title">Datos del producto</h1>
        </div>
        <div class="card-body">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <fieldset><legend>Datos generales:</legend>
                <div class="row">
                <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
                        {!! Form::UTText('prod-name','nombre', 'Nombre', 'Nombre del producto', $data->nombre, $errors, 40, true) !!}
                    </div>
                    <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
                        {!! Form::UTText('prod-com','marca', 'Marca', 'Marca del producto', $data->marca, $errors, 40, true) !!}
                    </div>
                    <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
                        {!! Form::UTText('prod-des','descripcion', 'Descripción', 'Descripción del producto', $data->descripcion, $errors, 100, true) !!}
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                        {!! Form::UTNumeric('prod-stock','stock', 'Stock', 'Cantidad disponible del producto', $data->stock, $errors, 4, true) !!}
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                        {!! Form::UTNumeric('prod-price','precio', 'Precio', 'Precio del producto', $data->precio, $errors, 10, true) !!}
                    </div>
                    <div class="col-lg-2 col-md-6 col-sm-6 col-xs-12">
                        {!! Form::UTImage('assets/uploads/'.$data->imagen, '100px', '100px')!!}
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                        {!! Form::UTFile('imagen', 'Imagen', 'imagen del producto', true) !!}
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <button data-cy="submit" type="submit" href="" class="btn btn-outline-info" title="Guardar">Guardar</button>
                    </div>
                </div>
                </fieldset>
            </div>
        </div>
        {!! Form::close() !!}
    </div>

@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
    <script src="{{URL::asset('js/validatorFields.js')}}"></script>
@stop
