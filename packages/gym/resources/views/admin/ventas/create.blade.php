
@extends('adminlte::page')

@section('title', 'Ventas')

@section('content_header')
    <h1>Ventas -> Nueva venta</h1>
@stop

@section('content') 
    <div class="card">
        {!! Form::open(['route' => 'ventas.store', 'enctype' => "multipart/form-data", 'method' => 'post']) !!}
        {!! Form::hidden('create_by', Auth::user()->name )!!}
        <div class="card-header">
            <h1 class="card-title">Datos de la venta</h1>
        </div>
        <div class="card-body">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <fieldset><legend>Datos generales:</legend>
                <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <label class='form-label' title='Producto'>Producto a vender<span class=\"required\" style='color:red!important;'>&nbsp;<strong>*</strong></span></label></label>
                    <select data-cy="sale-prod" class="form-control" aria-label="Default select example" name="id_producto">
                    <option selected>Seleccione un producto</option>
                    @foreach($productos as $producto)
                    <option value="{{ $producto->id_producto }}">{{ $producto->nombre }}</option>
                        @endforeach
                    </select>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <label class='form-label' title='Cliente'>Cliente que compra<span class=\"required\" style='color:red!important;'>&nbsp;<strong>*</strong></span></label></label>
                    <select data-cy="sale-client" class="form-control" aria-label="Default select example" name="id_cliente">
                    <option selected>Seleccione un cliente</option>
                    @foreach($clientes as $cliente)
                    <option value="{{ $cliente->id_cliente }}">{{ $cliente->nombre }}</option>
                        @endforeach
                        <option value="">Otro(cliente externo)</option>
                    </select>
                    </div>
                    <br>
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                        {!! Form::UTText('sale-cant','cantidad', 'Cantidad', 'Cantidad', null, $errors, 40, true) !!}
                    </div>
                    
                   
                </div>
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <button data-cy="submit" type="submit" href="" class="btn btn-outline-info" title="Guardar">Guardar</button>
                    </div>
                </div>
                </fieldset>
            </div>
        </div>
        {!! Form::close() !!}
    </div>

@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
    <script src="{{URL::asset('js/validatorFields.js')}}"></script>
@stop
