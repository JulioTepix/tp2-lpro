
@extends('adminlte::page')

@section('title', 'Ventas')

@section('content_header')
    <h1>Ventas de hoy <?php
    date_default_timezone_set('America/Mexico_City');
    echo date("d-m-20y");
    ?></h1>
@stop

@section('content')
<div class="card">
        <div class="header">
        
            
        </div>
       
<!-- Hover Rows -->
<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            <div class="body table-responsive">

                <!-- Buscador  -->
                <div class="row">
                    <div class="col-md-7 col-sm-7 col-xs-12">
                        <h5><strong>{{$data->total()}}</strong> Registro(s) encontrado(s).
                            P&aacute;gina
                            <strong>{{($data->total()==0) ? '0' : $data->currentPage()}}</strong> de
                            <strong> {{$data->lastPage()}}</strong>.
                            Registros por p&aacute;gina
                            <strong>{{($data->total()==0) ? '0' : $data->perPage()}}</strong></h5>
                            <?php
                            $ga = 0;
                                foreach($data as $row){
                                    $ga+=$row->total_pago;
                                }
                            ?>
                            <h5>Ganancia por venta de productos del dia: ${{ $ga }} pesos
                           
                    </div>
                    <div class="title_right">
                        <div class="col-md-10 col-sm-8 col-xs-12 form-group pull-right top_search">

                            <form action="{{route('ventas.index')}}" method="get">
                                <div class="input-group">
                                    <input data-cy="search-sale" type="text" class="form-control" name="search"
                                           value="{{$search}}"
                                           placeholder="Nombre cliente" style="border: 1px solid #ccc!important;width: 98%;">
                                    <span class="input-group-btn">
                                                <button data-cy="search" class="btn btn-outline-dark" type="submit">Buscar!</button>
                                            </span>
                                </div>
                            </form>

                        </div>
                    </div>
                </div>
                <a data-cy="new-sale" style="margin: 0 0 10px 85%" type="button" href="{{ route('ventas.create')}}" class="btn btn-outline-success">Nueva venta</a>
        
                <!-- Finalizar buscador  -->
                
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th style="text-align: center;">Cliente</th>
                        <th style="text-align: center;">Producto</th>
                        <th style="text-align: center;">Cantidad</th>
                        <th style="text-align: center;">Pago Total</th>
                        <th style="text-align: center;">Venta realizada<br>por</th>
                        <th style="text-align: center;">Opciones</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($data as $row)
                        <tr>
                            <td style="text-align: center;">{{$row->nombre}}</td>
                            <td style="text-align: center;">{{$row->producto}}</td>
                            <td style="text-align: center;">{{$row->cantidad}}</td>
                            <td style="text-align: center;">{{$row->total_pago}}</td>
                            <td style="text-align: center;">{{$row->create_by}}</td>
                            <td style="text-align: center;">
                                <div class="btn-group" style="display: inline!important;">
                                    <a data-cy="delete-sale" class='btn btn-outline-danger' style="float: none!important;" href="{{route('ventas.delete', $row->id_ventas)}}" title='Eliminar registro'><i class="material-icons">Eliminar</i></a>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>

                <!-- Paginador -->
            {!! $data->setPath(route('ventas.index'))->appends(Request::except('page'))->render() !!}
            <!-- Finalizar paginador -->

            </div>
        </div>
    </div>
</div>
    </div>
@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
    <script> console.log('Hi!'); </script>
@stop
