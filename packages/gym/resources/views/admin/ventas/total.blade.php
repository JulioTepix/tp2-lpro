@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <h1>PROCESO EXITOSO</h1>
@stop

@section('content')
    <h1>
        
    Total a pagar : {{$dataVentas['total_pago']}} €</h1>
@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
    <script> console.log('Hi!'); </script>
    
@stop