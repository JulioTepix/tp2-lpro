
@extends('adminlte::page')

@section('title', 'Suscripciones')

@section('content_header')
    <h1>Suscripciones</h1>
@stop

@section('content')
<div class="card">
        <div class="header">
        
            
        </div>
       
<!-- Hover Rows -->
<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            

                <!-- Buscador  -->
                
                <a data-cy="new-sus" style="margin: 0 0 10px 85%" type="button" href="{{ route('suscripciones.create')}}" class="btn btn-outline-success">Nuevo registro</a>
        
                <!-- Finalizar buscador  -->
                
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th style="text-align: center;">Tipo de suscripción</th>
                        <th style="text-align: center;">Precio</th>
                        <th style="text-align: center;">Opciones</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($data as $row)
                        <tr>
                            <td style="text-align: center;">{{$row->tipo_suscripcion}}</td>
                            <td style="text-align: center;">{{$row->precio}} €</td>
                            <td style="text-align: center;">
                                <div class="btn-group" style="display: inline!important;">
                                    <a data-cy="edit-sus" class='btn btn-outline-primary' style="float: none!important;" href="{{route('suscripciones.update', $row->id_suscripcion)}}" title='Editar registro'><i class="material-icons">Editar</i></a>
                                    <a data-cy="delete-sus" class='btn btn-outline-danger' style="float: none!important;" href="{{route('suscripciones.delete', $row->id_suscripcion)}}" title='Eliminar registro'><i class="material-icons">Eliminar</i></a>

                                </div>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
        </div>
    </div>
</div>
    </div>
@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
    <script> console.log('Hi!'); </script>
@stop
