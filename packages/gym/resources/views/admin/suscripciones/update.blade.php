
@extends('adminlte::page')

@section('title', 'Suscripción')

@section('content_header')
    <h1>Suscripción -> Editar</h1>
@stop

@section('content') 
    <div class="card">
        {!! Form::model($data,['route' => ['suscripciones.edit', $data->id_suscripcion], 'enctype' => "multipart/form-data", 'file' => true, 'method' => 'put']) !!}
        {!! Form::hidden('id_suscripcion', $data->id_suscripcion)!!}
        
        <div class="card-header">
            +
        </div>
        <div class="card-body">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <fieldset><legend>Datos generales:</legend>
                <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                {!! Form::UTText('sus-type','tipo_suscripcion', 'Tipo de suscripción', 'Tipo de suscripción', $data->tipo_suscripcion, $errors, 30, true) !!}
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    {!! Form::UTNumeric('sus-price','precio', 'Precio', 'Precio', $data->precio, $errors, 10, true) !!}
                    </div>
                </div>
                </div>
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <button data-cy="submit" type="submit" href="" class="btn btn-outline-info" title="Guardar">Guardar</button>
                    </div>
                </div>
                </fieldset>
            </div>
        </div>
        {!! Form::close() !!}
    </div>

@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
    <script src="{{URL::asset('js/validatorFields.js')}}"></script>
@stop
