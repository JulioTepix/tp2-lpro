@extends('adminlte::page')

@section('title', 'Suscripciones')

@section('content_header')
    <h1>Suscripciones</h1>
@stop

@section('content')
<div class="card">
{!! Form::open(['route' => 'suscripciones.destroy', 'method' => 'post']) !!}
{!! Form::hidden('id_suscripcion',$data->id_suscripcion) !!}
    <div class="card-header" >
        <h1 class="card-title">Datos del cliente</h1>
    </div>
    <div class="card-body">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <H1>¿Seguro que desea eliminar este registro?</H1>
                    <a type="button" href="{{ route('suscripciones.index')}}" class="btn btn-outline-dark">No</a>
                    <button data-cy="conf-delete" type="submit" href="" class="btn btn-outline-dark" title="Delete">Si</button>
                    
                </div>
        </div>
    </div>
</div>
@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
    <script> console.log('Hi!'); </script>
@stop
