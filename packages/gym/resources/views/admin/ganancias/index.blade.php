
@extends('adminlte::page')

@section('title', 'Ganancias')

@section('content_header')
    <h1>Ganancias de hoy </h1>
@stop

@section('content')
<div class="card">
        <div class="header">
        
            
        </div>
       
<!-- Hover Rows -->
<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            
        <div class="title_right">
        <div class="col-md-12 col-sm-12 col-xs-12">

            
        {!! Form::open(['route' => 'ganancias.index', 'enctype' => "multipart/form-data", 'method' => 'get']) !!}

        {!! Form::UTCalendar('search-profit','search', 'Ganancias en otros dias', 'busqueda por fecha', '0000-00-00', $errors) !!}
        
        <span class="input-group-btn">
                    <button data-cy="search" class="btn btn-outline-dark" type="submit">Buscar!</button>
                </span>
    </div>
                
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th style="text-align: center;">Ganancias por productos</th>
                        <th style="text-align: center;">Ganancias por suscripciones</th>
                        <th style="text-align: center;">Ganancias por visitas</th>
                        <th style="text-align: center;">=</th>
                        <th style="text-align: center;">Total</th>
                    </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td style="text-align: center;">{{$GVentas}} €</td>
                            <td style="text-align: center;">{{$GSus}} €</td>
                            <td style="text-align: center;">{{$GVisitas}} €</td>
                            <th style="text-align: center;">=</th>
                            <td style="text-align: center;">{{$total}} €</td>
                        </tr>
                    </tbody>
                </table>
        </div>
    </div>
</div>
    </div>
@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
    <script> console.log('Hi!'); </script>
@stop
