@extends('adminlte::page')

@section('title', 'Clientes')

@section('content_header')
    <h1>Clientes</h1>
@stop

@section('content')
<div class="card">
{!! Form::open(['route' => 'clientes.destroy', 'method' => 'post']) !!}
{!! Form::hidden('id_cliente',$data->id_cliente) !!}
    <div class="card-header" >
        <h1 class="card-title">Datos del cliente</h1>
    </div>
    <div class="card-body">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
            <fieldset><Legend>Datos generales:</Legend>
            <div class="row">
              </div>
                </div>
                <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
                    <fieldset><Legend>Datos del cliente:</Legend>
                    </div>
                    <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
                    <label>Nombre: </label><p>{{$data->nombre}}</p>
                    </div>
                    <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
                    <label>Apellido Paterno: </label><p>{{$data->apellido_paterno}}</p>
                    </div>
                    <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
                    <label>Apellido Materno: </label><p>{{$data->apellido_materno}}</p>
                    </div>
                    <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
                    <label>Edad: </label><p>{{$data->edad}}</p>
                    </div>
                    <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
                    <label>Telefono: </label><p>{{$data->telefono}}</p>
                    </div>
                    <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
                    <label>Fecha de inscripción: </label><p>{{$data->fecha_inscripcion}}</p>
                    </div>
                    <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
                    <label>Fecha de próximo pago: </label><p>{{$data->fecha_proximo_pago}}</p>
                    </div>
                    <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
                    <label>Fecha de próximo pago: </label><p>{{$data->fecha_proximo_pago}}</p>
                    </div>
                    <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
                    <label>Tipo de suscripción: </label><p>{{$data->tipo_suscripcion}}</p>
                    </div>
                    
                </div>
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <H1>¿Seguro que desea eliminar este registro?</H1>
                    <a type="button" href="{{ route('clientes.index')}}" class="btn btn-outline-dark">No</a>
                    <button data-cy="conf-delete" type="submit" class="btn btn-outline-dark" title="Delete">Si</button>
                    </div>
                </div>
            </fieldset>
        </div>
    </div>
</div>
@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
    <script> console.log('Hi!'); </script>
@stop
