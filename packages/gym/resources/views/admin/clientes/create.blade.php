@extends('adminlte::page')

@section('title', 'Clientes')

@section('content_header')
    <h1>Clientes -> Nuevo Gym bro</h1>
@stop

@section('content') 
    <div class="card">
        {!! Form::open(['route' => 'clientes.store', 'enctype' => "multipart/form-data", 'file' => true, 'method' => 'post']) !!}
        <div class="card-header">
            <h1 class="card-title">Datos del cliente</h1>
        </div>
        <div class="card-body">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <fieldset><legend>Datos generales:</legend>
                <div class="row">
                <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
                        {!! Form::UTText('client-name', 'nombre', 'Nombre', 'Nombre del cliente', null, $errors, 40, true) !!}
                    </div>
                    <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
                        {!! Form::UTText('client-lastn1','apellido_paterno', 'Apellido Paterno', 'Apellido paterno del cliente', null, $errors, 40, true) !!}
                    </div>
                    <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
                        {!! Form::UTText('client-lastn2','apellido_materno', 'Apellido Materno', 'Apellido materno del cliente', null, $errors, 40, true) !!}
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                        {!! Form::UTNumeric('client-age','edad', 'Edad', 'Edad del Gym bro', null, $errors, 2, true) !!}
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                        {!! Form::UTText('client-tel','telefono', 'Telefono', 'Numero de telefono', null, $errors, 10) !!}
                    </div>
                    <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
                        {!! Form::UTCalendar('client-DP','fecha_pago', 'Fecha que realiza el pago', 'Fecha del último pago', null, $errors, true) !!}
                    </div>
                    <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
                        {!! Form::UTCalendar('client-DPP','fecha_proximo_pago', 'Fecha del próximo pago', 'Fecha del proximo pago', null, $errors, true) !!}
                    </div>
                    <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
                    <label class='form-label' title='Producto'>Tipo de suscripción<span class=\"required\" style='color:red!important;'>&nbsp;<strong>*</strong></span></label></label>
                    <select data-cy="type-sus" class="form-control" aria-label="Default select example" name="id_suscripcion">
                    <option selected>Seleccione una suscripción</option>
                    @foreach($data as $ss)
                    <option value="{{ $ss->id_suscripcion }}">{{ $ss->tipo_suscripcion }}</option>
                        @endforeach
                    </select>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <button data-cy="submit" type="submit" href="" class="btn btn-outline-info" title="Guardar">Guardar</button>
                    </div>
                </div>
                </fieldset>
            </div>
        </div>
        {!! Form::close() !!}
    </div>

@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
    <script src="{{URL::asset('js/validatorFields.js')}}"></script>
@stop
