
@extends('adminlte::page')

@section('title', 'Clientes')

@section('content_header')
    <h1>Clientes</h1>
@stop

@section('content')
<div class="card">
        <div class="header">
        
            
        </div>
       
<!-- Hover Rows -->
<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            <div class="body table-responsive">

                <!-- Buscador  -->
                <div class="row">
                    <div class="col-md-7 col-sm-7 col-xs-12">
                        <h5><strong>{{$data->total()}}</strong> Registro(s) encontrado(s).
                            P&aacute;gina
                            <strong>{{($data->total()==0) ? '0' : $data->currentPage()}}</strong> de
                            <strong> {{$data->lastPage()}}</strong>.
                            Registros por p&aacute;gina
                            <strong>{{($data->total()==0) ? '0' : $data->perPage()}}</strong></h5>
                           
                            
                        <svg xmlns="http://www.w3.org/2000/svg" width="30" height="30" fill="currentColor" class="bi bi-info" viewBox="0 0 16 16">
                        <path d="m8.93 6.588-2.29.287-.082.38.45.083c.294.07.352.176.288.469l-.738 3.468c-.194.897.105 1.319.808 1.319.545 0 1.178-.252 1.465-.598l.088-.416c-.2.176-.492.246-.686.246-.275 0-.375-.193-.304-.533L8.93 6.588zM9 4.5a1 1 0 1 1-2 0 1 1 0 0 1 2 0z"/>
                        </svg>El semáforo de suscripción representa lo siguiente:</p>
                        <ul>
                            <li><a class="text-green">Verde</a>: Faltan más de 10 dias para su próximo pago.</li>
                            <li><a class="text-yellow">Amarillo</a>: Faltan entre 4 y 10 dias para su próximo pago.</li>
                            <li><a class="text-red">Rojo</a>: Faltan menos de 3 dias para su próximo pago.</li>
                            <li><a class="text-gray">Gris</a>: La fecha de pago ya pasó (si el cliente no continuó la suscripción, se recomienda eliminar el registro días despues)</li>
                        </ul>
                    </div>
                    <div class="title_right">
                        <div class="col-md-10 col-sm-8 col-xs-12 form-group pull-right top_search">

                            <form action="{{route('clientes.index')}}" method="get">
                                <div class="input-group">
                                    <input data-cy="search-client" type="text" class="form-control" name="search"
                                           value="{{$search}}"
                                           placeholder="Nombre/Tipo de suscripción" style="border: 1px solid #ccc!important;width: 98%;">
                                    <span class="input-group-btn">
                                                <button data-cy="search" class="btn btn-outline-dark" type="submit">Buscar!</button>
                                            </span>
                                </div>
                            </form>

                        </div>
                    </div>
                </div>
                <a data-cy="new-client" style="margin: 0 0 10px 85%" type="button" href="{{ route('clientes.create')}}" class="btn btn-outline-success">Nuevo cliente</a>
        
                <!-- Finalizar buscador  -->
                
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th style="text-align: center;">Semaforo de<br> suscripción</th>
                        <th style="text-align: center;">Nombre</th>
                        <th style="text-align: center;">Tipo de suscripción</th>
                        <th style="text-align: center;">Fecha de <br>próximo pago</th>
                        <th style="text-align: center;">Opciones</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($data as $row)
                    <?php 
                    date_default_timezone_set('America/Mexico_City');
                    $fecha = $row->fecha_proximo_pago;
                    $fechahoy =  date("Y-m-d");
                    $resta = strtotime($fechahoy)-strtotime($fecha);
                    $dias = $resta / (24*3600);
                    $semaforo = ($dias*-1);
                        ?>
                        <tr>
                            @if($semaforo >= 0 and $semaforo <= 3)
                            <td style="text-align: center; background-color: red"></td>
                            @endif
                            @if($semaforo > 3 and $semaforo <= 10)
                            <td style="text-align: center; background-color: yellow"></td>
                            @endif
                            @if($semaforo > 10)
                            <td style="text-align: center; background-color: green"></td>
                            @endif
                            @if($semaforo < 0)
                            <td style="text-align: center; background-color: gray"></td>
                            @endif
                            <td style="text-align: center;">{{$row->nombre}}</td>
                            <td style="text-align: center;">{{$row->tipo_suscripcion}}</td>
                            <td style="text-align: center;">{{$row->fecha_proximo_pago}}</td>
                            <td style="text-align: center;">
                                <div class="btn-group" style="display: inline!important;">
                                    <a data-cy="show-client" class='btn btn-outline-warning' style="float: none!important;" href="{{route('clientes.show', $row->id_cliente)}}" title='Mostrar registro'><i class="material-icons">Mostrar</i></a>
                                    <a data-cy="edit-client" class='btn btn-outline-primary' style="float: none!important;" href="{{route('clientes.update', $row->id_cliente)}}" title='Editar registro'><i class="material-icons">Editar</i></a>
                                    <a data-cy="delete-client" class='btn btn-outline-danger' style="float: none!important;" href="{{route('clientes.delete', $row->id_cliente)}}" title='Eliminar registro'><i class="material-icons">Eliminar</i></a>

                                </div>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>

                <!-- Paginador -->
            {!! $data->setPath(route('clientes.index'))->appends(Request::except('page'))->render() !!}
            <!-- Finalizar paginador -->

            </div>
        </div>
    </div>
</div>
    </div>
@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
    <script> console.log('Hi!'); </script>
@stop
