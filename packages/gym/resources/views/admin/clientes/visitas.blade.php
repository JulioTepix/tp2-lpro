
@extends('adminlte::page')

@section('title', 'Visitas')

@section('content_header')
    <h1>Clientes -> Visitantes</h1>
@stop

@section('content')
<div class="card">
        <div class="header">
        
            
        </div>
       
<!-- Hover Rows -->
<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            <div class="body table-responsive">

                <!-- Buscador  -->
                <div class="row">
                    <div class="col-md-7 col-sm-7 col-xs-12">
                        <h5><strong>{{$data->total()}}</strong> Registro(s) encontrado(s).
                            P&aacute;gina
                            <strong>{{($data->total()==0) ? '0' : $data->currentPage()}}</strong> de
                            <strong> {{$data->lastPage()}}</strong>.
                            Registros por p&aacute;gina
                            <strong>{{($data->total()==0) ? '0' : $data->perPage()}}</strong></h5>
                    </div>
                </div>
                <!-- Finalizar buscador  -->
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th style="text-align: center;">Nombre</th>
                        <th style="text-align: center;">Fecha de Visita</th>
                        <th style="text-align: center;">Edad</th>}
                        <th style="text-align: center;">Telefono</th>
                        <th style="text-align: center;">Opciones</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($data as $row)
                        <tr>
                            <td style="text-align: center;">{{$row->nombre}}</td>
                            <td style="text-align: center;">{{$row->fecha_pago}}</td>
                            <td style="text-align: center;">{{$row->edad}}</td>
                            <td style="text-align: center;">{{$row->telefono}}</td>
                            <td style="text-align: center;">
                                <div class="btn-group" style="display: inline!important;">
                                    <a class='btn btn-outline-primary' style="float: none!important;" href="{{route('clientes.update', $row->id_cliente)}}" title='Editar registro'><i class="material-icons">Editar</i></a>
                                    <a class='btn btn-outline-danger' style="float: none!important;" href="{{route('clientes.delete', $row->id_cliente)}}" title='Eliminar registro'><i class="material-icons">Eliminar</i></a>

                                </div>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>

                <!-- Paginador -->
            {!! $data->setPath(route('clientes.index'))->appends(Request::except('page'))->render() !!}
            <!-- Finalizar paginador -->

            </div>
        </div>
    </div>
</div>
    </div>
@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
    <script> console.log('Hi!'); </script>
@stop
