<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\GananciasRepository;
date_default_timezone_set('America/Mexico_City');

class GananciasController extends Controller
{
    private $ga;

    public function __construct(GananciasRepository $clien){
        $this->ga = $clien;
    }

    public function index(Request $request){
        $GVentas = 0;
        $GSus = 0;
        $GVisitas = 0;
        $hoy = date("y-m-d");
        if($request->has('search')){
            $search = $request->input('search');
            if(trim($search) != ''){
                $ventas = $this->ga->venta($search);
                $suscripciones = $this->ga->suscripcion($search);
                $visitas = $this->ga->visita($search);
            }else{
                $ventas = $this->ga->venta($hoy);
                $suscripciones = $this->ga->suscripcion($hoy);
                $visitas = $this->ga->visita($hoy);
            }
        }else{
            $ventas = $this->ga->venta($hoy);
            $suscripciones = $this->ga->suscripcion($hoy);
            $visitas = $this->ga->visita($hoy);
        }

        foreach($ventas as $venta){
            $GVentas += $venta->total_pago;
        }

        foreach($suscripciones as $suscripcion){
            $GSus += $suscripcion->precio;
        }

        foreach($visitas as $visita){
            $GVisitas += $visita->precio;
        }

        $total = $GVentas + $GSus + $GVisitas;

        return view('admin.ganancias.index', ['GVentas' => $GVentas,'GSus' => $GSus,'GVisitas' => $GVisitas, 'total' => $total]);
    }  
    
}