<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\ProductosRepository;
use Illuminate\Support\Facades\Session;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Pagination\Paginator;

class ProductosController extends Controller
{
    private $producto;

    public function __construct(ProductosRepository $productosss){
        $this->producto = $productosss;
    }

    public function index(Request $request){
        $search = "";
        $limit =10;
        if($request->has('search')){
            $search = $request->input('search');
            if(trim($search) != ''){
                $data = $this->producto->search($search);
            }else{
                $data = $this->producto->all();
            }
        }else{
            $data = $this->producto->all();
        }
        $currentPage = Paginator::resolveCurrentPage() -1;
        $perPage = $limit;
        $currentPageSearchResults = $data->slice($currentPage * $perPage, $perPage)->all();
        $data = new LengthAwarePaginator($currentPageSearchResults, count($data), $perPage);
        return view('admin.productos.index', ['data' => $data, 'search' => $search, 'page' => $currentPage]);
    }

    public function create(){
        return view('admin.productos.create');
    }

    public function store(Request $request){
        
        $searchString = "";
        $replaceString = "";
        $originalString = $request['nombre'].'_';
        $nameimage = str_replace($searchString, $replaceString, $originalString);
            $mimearray = array('jpg','png','jpeg');
            
            if ($request->file('imagen') != null) {
                $extension = strtolower($request->file('imagen')->getClientOriginalExtension());//obtenemos el nombre de la
                if (in_array($extension, $mimearray)) {
                    $size = getimagesize($request->file('imagen'));
                    //dd($size);
                    if ($size[0] <= 1500 && $size[1] <= 1500) {
                        $weight = $_FILES['imagen']['size'];
                        if ($weight < 2000000) {
                            //Guardamos la imagen
                            $name = $nameimage . date("d") . date("H") . date("i") . date("s") . '.' . $extension;//renombramos la imagen
                            $path = public_path('assets/uploads/');//obtenemor la ruta del archivo
                            $request->file('imagen')->move($path, $name);//copiamos el archivo a una carpeta del proyecto
                            $value = $name;
                            
                            $dataProducto = [
                                'nombre' => $request['nombre'],
                                'marca' => $request['marca'],
                                'descripcion' => $request['descripcion'],
                                'imagen' => $value,
                                'stock' => $request['stock'],
                                'precio' => $request['precio']
                            ];
                            $result = $this->producto->store($dataProducto);
                            
                            if($result == 1){
                                Session::flash('status', 'Los datos han sido guardados');
                                Session::flash('status_type', 'success');
                                return redirect()->route('productos.index');
                            }
                            if($result == 2){
                                Session::flash('status', 'Problema del proceso');
                                Session::flash('status_type', 'danger');
                                return redirect()->route('products.create');
                            }
                            if($result == 3){
                                Session::flash('status', 'Problema del query');
                                Session::flash('status_type', 'danger');
                                return redirect()->route('products.create2');
                            }

                           
                        } else {
                            Session::flash('status', 'La imagen no pudo ser guardada por tamaño');
                            Session::flash('status_type', 'warning');
                            return back()->withInput();}
                    } else {
                        Session::flash('status', 'La imagen no pudo ser guardada por dimension');
                        Session::flash('status_type', 'warning');
                        return back()->withInput();}
                } else {
                    Session::flash('status', 'La imagen no pudo ser guardada por formato');
                    Session::flash('status_type', 'warning');
                    return back()->withInput();
                }
            } else {
                Session::flash('status', 'La imagen no pudo ser guardada por existencia');
                Session::flash('status_type', 'warning');
                return back()->withInput();
            }
    }

    public function show($id){
        $data = $this->producto->show($id);
        return view('admin.productos.show',['data'=>$data]);
    }

    public function update($id){
        $data = $this->producto->show($id);
        return view('admin.productos.update',['data'=>$data]);
    }

    public function edit(Request $request, $id){
        if ($request->file('imagen') != null) {
            $searchString = " ";
            $replaceString = "";
            $originalString = $request['nombre'].'_';
            $nameimage = str_replace($searchString, $replaceString, $originalString);
            $mimearray = array('jpg', 'png');
            $extension = strtolower($request->file('imagen')->getClientOriginalExtension());//obtenemos el nombre de la
            if (in_array($extension, $mimearray)) {
                $size = getimagesize($request->file('imagen'));
                if ($size[0] <= 1500 && $size[1] <= 1500) {
                    $weight = $_FILES['imagen']['size'];
                    if ($weight < 2000000) {
                        //Guardamos la imagen
                        $name = $nameimage . date("d") . date("H") . date("i") . date("s") . '.' . $extension;//renombramos la imagen
                        $path = public_path('assets/uploads/productos/');//obtenemor la ruta del archivo
                        $request->file('imagen')->move($path, $name);//copiamos el archivo a una carpeta del proyecto
                        $value = $name;
                        $dataProducto = [
                            'nombre' => $request['nombre'],
                            'marca' => $request['marca'],
                            'descripcion' => $request['descripcion'],
                            'imagen' => $value,
                            'stock' => $request['stock'],
                            'precio' => $request['precio']
                        ];
                        $result = $this->producto->update($dataProducto, $id);
        
                        if($result == 1){
                            Session::flash('status', 'El producto fue editado');
                            Session::flash('status_type', 'success');
                            return redirect()->route('productos.index');
                        }
                        if($result == 2){
                            Session::flash('status', 'Problema del proceso');
                            Session::flash('status_type', 'danger');
                            return back()->withInput();
                        }
                        if($result == 3){
                            Session::flash('status', 'Problema del query');
                            Session::flash('status_type', 'danger');
                            return back()->withInput();
                        }
        
        
                    } else {
                        Session::flash('status', 'La imagen del producto no pudo ser guardada ya que presenta un problema de '.$value);
                        Session::flash('status_type', 'warning');
                        return back()->withInput();
                    }
                } else {
                    Session::flash('status', 'La imagen del producto no pudo ser guardada ya que presenta un problema de '.$value);
                    Session::flash('status_type', 'warning');
                    return back()->withInput();
                }
            } else {
                Session::flash('status', 'La imagen del producto no pudo ser guardada ya que presenta un problema de '.$value);
                Session::flash('status_type', 'warning');
                return back()->withInput();
            }
        }else {
            $dataProducto = [
                'nombre' => $request['nombre'],
                'marca' => $request['marca'],
                'descripcion' => $request['descripcion'],
                'stock' => $request['stock'],
                'precio' => $request['precio']
            ];
            $result = $this->producto->update($dataProducto, $id);
        
            if($result == 1){
                Session::flash('status', 'El producto fue editado');
                Session::flash('status_type', 'success');
                return redirect()->route('productos.index');
            }
            if($result == 2){
                Session::flash('status', 'Problema del proceso');
                Session::flash('status_type', 'danger');
                return back()->withInput();
            }
            if($result == 3){
                Session::flash('status', 'Problema del query');
                Session::flash('status_type', 'danger');
                return back()->withInput();
            }
        }
    }

    public function delete($id){
        $data = $this->producto->show($id);
        return view('admin.productos.delete',['data'=>$data]);
    }

    public function destroy(Request $request){
        $id = $request['id_producto'];
        $result = $this->producto->destroy($id);
        if($result == 1){
            Session::flash('status', 'La producto fue eliminada');
            Session::flash('status_type', 'success');
            return redirect()->route('productos.index');
        }
        if($result == 2){
            Session::flash('status', 'Problema del proceso');
            Session::flash('status_type', 'danger');
            return back()->withInput();
        }
        if($result == 3){
            Session::flash('status', 'Problema del query');
            Session::flash('status_type', 'danger');
            return back()->withInput();
        }  
    }
}
