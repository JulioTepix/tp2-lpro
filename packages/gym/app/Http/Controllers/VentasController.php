<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\VentasRepository;
use Illuminate\Support\Facades\Session;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Pagination\Paginator;

class VentasController extends Controller
{
    private $ventas;

    public function __construct(VentasRepository $vetas){
        $this->ventas = $vetas;
    }

    public function index(Request $request){
        $search = "";
        $limit =5;
        if($request->has('search')){
            $search = $request->input('search');
            if(trim($search) != ''){
                $data = $this->ventas->search($search);
            }else{
                $data = $this->ventas->all();
            }
        }else{
            $data = $this->ventas->all();
        }
        $currentPage = Paginator::resolveCurrentPage() -1;
        $perPage = $limit;
        $currentPageSearchResults = $data->slice($currentPage * $perPage, $perPage)->all();
        $data = new LengthAwarePaginator($currentPageSearchResults, count($data), $perPage);
        return view('admin.ventas.index', ['data' => $data, 'search' => $search, 'page' => $currentPage]);
    }

    public function create(){
        $clientes = $this->ventas->clientes();
        $productos = $this->ventas->productos();
        return view('admin.ventas.create', ['clientes' => $clientes, 'productos' => $productos]);
    }

    public function store(Request $request){
                        $precio = $this->ventas->precio($request['id_producto']);
                     $total = $precio * $request['cantidad'];
                $dataVentas = [
                'id_producto' => $request['id_producto'],
                'id_cliente' => $request['id_cliente'],
                'cantidad' => $request['cantidad'],
                'total_pago' => $total,
                'create_by' => $request['create_by'],
                ];

                            $result = $this->ventas->store($dataVentas);
                            
                            if($result == 1){
                                Session::flash('status', 'Los datos han sido guardados');
                                Session::flash('status_type', 'success');
                                return view('admin.ventas.total',['dataVentas' => $dataVentas]);
                            }
                            if($result == 2){
                                Session::flash('status', 'Problema del proceso');
                                Session::flash('status_type', 'danger');
                                return view('admin.alerts.error');
                            }
                            if($result == 3){
                                Session::flash('status', 'Problema del query');
                                Session::flash('status_type', 'danger');
                                return view('admin.alerts.error');
                            }
            
    }

    public function show($id){
        $data = $this->ventas->show($id);
        return view('admin.ventas.show',['data'=>$data]);
    }

    public function update($id){
        $data = $this->ventas->show($id);
        $clientes = $this->ventas->clientes();
        $productos = $this->ventas->productos();
        return view('admin.ventas.update',['data'=>$data, 'clientes' => $clientes, 'productos' => $productos]);
    }

    public function edit(Request $request, $id){
     
        $dataVentas = [
            'id_producto' => $request['id_producto'],
            'id_cliente' => $request['id_cliente'],
            'cantidad' => $request['cantidad']
        ];
        $result = $this->ventas->update($dataVentas, $id);
        
        if($result == 1){
            Session::flash('status', 'Los datos han sido guardados');
            Session::flash('status_type', 'success');
            return view('admin.ventas.total',['dataVentas' => $dataVentas]);
        }
        if($result == 2){
            Session::flash('status', 'Problema del proceso');
            Session::flash('status_type', 'danger');
            return view('admin.alerts.error');
        }
        if($result == 3){
            Session::flash('status', 'Problema del query');
            Session::flash('status_type', 'danger');
            return view('admin.alerts.error');
        }
    }

    public function delete($id){
        $data = $this->ventas->show($id);
        return view('admin.ventas.delete',['data'=>$data]);
    }

    public function destroy(Request $request){
        $id = $request['id_ventas'];
        $result = $this->ventas->destroy($id);
        if($result == 1){
            Session::flash('status', 'La ventas fue eliminada');
            Session::flash('status_type', 'success');
            return redirect()->route('ventas.index');
        }
        if($result == 2){
            Session::flash('status', 'Problema del proceso');
            Session::flash('status_type', 'danger');
            return back()->withInput();
        }
        if($result == 3){
            Session::flash('status', 'Problema del query');
            Session::flash('status_type', 'danger');
            return back()->withInput();
        }  
    }
}