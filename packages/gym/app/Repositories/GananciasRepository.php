<?php
namespace App\Repositories;
use DB;

class GananciasRepository{

    public function venta($date){
        return DB::table('venta_completa')->where('fecha', '=', $date)->get();
    }

    public function suscripcion($date){
        return DB::table('clientes_sus')->where([['fecha_pago', '=', $date],['tipo_suscripcion','!=','Visit']])->get();
    }

    public function visita($date){
        return DB::table('clientes_sus')->where([['fecha_pago', '=', $date],['tipo_suscripcion','=','Visit']])->get();
    }
}