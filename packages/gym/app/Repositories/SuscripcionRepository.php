<?php
namespace App\Repositories;
use App\Models\Suscripcion;
use DB;

class SuscripcionRepository{
    public function all(){
        return Suscripcion::all();
    }

    public function store($data){
        try{
            DB::beginTransaction();
            $datass = [
                'tipo_suscripcion' => $data['tipo_suscripcion'],
                'precio' => $data['precio']
            ];
            $ss = new Suscripcion($datass);
            $ss->save();
            DB::commit();
            if($data['tipo_suscripcion'] == "Visit"){
                return 10;
            }else{
                return 1;
            }    
        }catch (\Exception $e){
            DB::rollBack();
            return 2;
        }catch (QueryException $ex){
            DB::rollBack();
            return 3;
        }
        
    }

    public function show($id){
        return Suscripcion::findOrFail($id);
    }

    public function update($data, $id){
        try{
            DB::beginTransaction();
           $ss = Suscripcion::findOrFail($id);
           $ss->update($data);
            DB::commit();
            return 1;
        }catch (\Exception $e){
            DB::rollBack();
            return 2;
        }catch (QueryException $ex){
            DB::rollBack();
            return 3;
        }
    }
    public function destroy($id){
        try{
            DB::beginTransaction();

            $ss = Suscripcion::findOrFail($id);
            $ss->delete();
    
            DB::commit();
            return 1;
        } catch (Exception $e){
            DB::rollBack();
            return 2;   
        } catch (QueryException $ex){
            DB::rollBack();
            return 3;
        }
    }
}