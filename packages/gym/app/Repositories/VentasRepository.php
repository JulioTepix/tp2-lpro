<?php
namespace App\Repositories;
use App\Models\Ventas;
use App\Models\Producto;
use DB;
date_default_timezone_set('America/Mexico_City');

class VentasRepository{
    public function all(){
        return DB::table('venta_completa')->
        where('fecha','=', date("y-m-d"))->orderby('id_ventas', 'DESC')->get();
    }

    public function clientes(){
        return DB::table('nombre_clientes')->orderby('nombre')->get();
    }
    
    public function productos(){
        return DB::table('producto')->where('estado','=', 1)
        ->orderby('nombre')->get();
    }

    public function precio($id){
        $data = Producto::findOrFail($id);
        return $data->precio;
    }

    public function search($value){
        return DB::table('venta_completa')->where([['nombre','like', "%$value%"]])
        ->orderby('nombre')->get();
    }

    public function store($data){
        try{
            DB::beginTransaction();
            $dataVentas = [
                'id_producto' => $data['id_producto'],
                'id_cliente' => $data['id_cliente'],
                'cantidad' => $data['cantidad'],
                'total_pago' => $data['total_pago'],
                'create_by' => $data['create_by'],
            ];
            $aide = Producto::findOrFail($data['id_producto']);
            $stock = $aide->stock - $data['cantidad'];
            Producto::where('id_producto', $data['id_producto'])->update(array('stock' => $stock));
            $ventas = new Ventas($dataVentas);
            $ventas->save();
            DB::commit();
            return 1;
        }catch (\Exception $e){
            DB::rollBack();
            return 2;
        }catch (QueryException $ex){
            DB::rollBack();
            return 3;
        }
        
    }

    public function show($id){
        return Ventas::findOrFail($id);
    }

    public function update($data, $id){
        try{
            DB::beginTransaction();
           $ventas = Ventas::findOrFail($id);
           $ventas->update($data);
            DB::commit();
            return 1;
        }catch (\Exception $e){
            DB::rollBack();
            return 2;
        }catch (QueryException $ex){
            DB::rollBack();
            return 3;
        }
    }
    public function destroy($id){
        try{
            DB::beginTransaction();
            $ventas = Ventas::findOrFail($id);
            $ventas->delete();
    
            DB::commit();
            return 1;
        } catch (Exception $e){
            DB::rollBack();
            return 2;   
        } catch (QueryException $ex){
            DB::rollBack();
            return 3;
        }
    }
}