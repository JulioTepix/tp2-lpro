<?php
namespace App\Repositories;
use App\Models\Cliente;
use DB;


class ClientesRepository{
    public function all_2(){
        return Cliente::all();
    }

    public function all(){
        return DB::table('clientes_sus')->where([['estado', '=', 1],['tipo_suscripcion','!=','Visit']])
        ->orderby('nombre')->get();
    }

    public function visitas(){
        return DB::table('clientes_sus')->where([['estado', '=', 1],['tipo_suscripcion','=','Visit']])->get();
    }

    public function suscripciones(){
        return DB::table('suscripciones')->get();
    }

    public function search($value){
        return DB::table('clientes_sus')->where([['nombre','like', "%$value%"],['estado','=',1]])
        ->orWhere([['tipo_suscripcion','like', "%$value%"],['estado','=',1]])
        ->orderby('nombre')->get();
    }

    public function store($data){
        try{

            DB::beginTransaction();

            $dataCliente = [
                'nombre' => $data['nombre'],
                'apellido_paterno' => $data['apellido_paterno'],
                'apellido_materno' => $data['apellido_materno'],
                'edad' => $data['edad'],
                'telefono' => $data['telefono'],
                'fecha_pago' => $data['fecha_pago'],
                'fecha_proximo_pago' => $data['fecha_proximo_pago'],
                'id_suscripcion' => $data['id_suscripcion'],
            ];
            $cliente = new Cliente($dataCliente);
            $cliente->save();
            DB::commit();
            $sss = DB::table('suscripciones')->where('tipo_suscripcion', '=', 'Visita')->get();
            foreach($sss as $s){
            if($data['id_suscripcion'] == $s->id_suscripcion){
                return 10;
            }else{
                return 1;
            }  
        }  
        }catch (\Exception $e){
            DB::rollBack();
            return 2;
        }catch (QueryException $ex){
            DB::rollBack();
            return 3;
        }
        
    }

    public function show($id){
        return Cliente::findOrFail($id);
    }

    public function update($data, $id){
        try{
            DB::beginTransaction();
           $cliente = Cliente::findOrFail($id);
           $cliente->update($data);
            DB::commit();
            return 1;
        }catch (\Exception $e){
            DB::rollBack();
            return 2;
        }catch (QueryException $ex){
            DB::rollBack();
            return 3;
        }
    }
    public function destroy($id){
        try{
            DB::beginTransaction();

            Cliente::where('id_cliente', $id)->update(array('estado' => 0));
    
            DB::commit();
            return 1;
        } catch (Exception $e){
            DB::rollBack();
            return 2;   
        } catch (QueryException $ex){
            DB::rollBack();
            return 3;
        }
    }
}