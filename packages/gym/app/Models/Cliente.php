<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Cliente extends Model
{
    protected $table = 'cliente';

    protected $primaryKey = 'id_cliente';
    public $timestamps = false;
    protected $fillable = [
        'id_cliente',
        'nombre',
        'apellido_paterno',
        'apellido_materno',
        'edad',
        'telefono',
        'fecha_inscripcion',
        'fecha_pago',
        'fecha_proximo_pago',
        'id_suscripcion',
        'estado'
    ];
}
