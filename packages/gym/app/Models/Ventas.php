<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Ventas extends Model
{
    protected $table = 'ventas';

    protected $primaryKey = 'id_ventas';
    public $timestamps = false;
    protected $fillable = [
        'id_ventas',
        'id_producto',
        'id_cliente',
        'cantidad',
        'total_pago',
        'create_by',
        'fecha'
    ];

    public function Producto()
    {
        return $this->hasMany('App\Models\Producto', 'id_producto');
    }

    public function Cliente()
    {
        return $this->hasMany('App\Models\Cliente', 'id_cliente');
    }
}
