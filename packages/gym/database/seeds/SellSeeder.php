<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SellSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i=0; $i < 10; $i++) {
        DB::table('ventas')->insert([
            'id_producto' => 1,
            'id_cliente' => rand(1,9),
            'cantidad' => 1,
            'total_pago' => 1.2
        ]);
        }
    }
}
