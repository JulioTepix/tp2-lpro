<?php
 
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

 
class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i=0; $i < 10; $i++) {
            DB::table('users')->insert([
                'name' => Str::random(10),
                'email' => Str::random(10).'@gmail.com',
                'password' => Hash::make('12341234'),
            ]);
        }

        DB::table('users')->insert([
            'name' => 'Julio',
            'email' => 'juputepix5@gmail.com',
            'password' => Hash::make('12345678'),
        ]);
    }
}
