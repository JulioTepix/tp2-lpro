<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ClientSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i=0; $i < 10; $i++) {
            DB::table('cliente')->insert([
                'nombre' => 'prénom'. $i,
                'apellido_paterno'  => 'nom'. $i,
                'apellido_materno'  => 'nom'. $i,
                'edad'  => $i+15,
                'telefono' => '076688541'.$i,
                'fecha_pago' => date('Y-m-d'),
                'id_suscripcion' => rand(1,4),
            ]);
        }

        DB::table('cliente')->insert([
            'nombre' => 'Julio',
            'apellido_paterno'  => 'Tepixtle',
            'apellido_materno'  => 'Hernandez',
            'edad'  => 20,
            'telefono' => '0766885412',
            'fecha_pago' => '2022-12-14',
            'fecha_proximo_pago' => '2023-01-15',
            'id_suscripcion' => 4,
        ]);
    }
}
