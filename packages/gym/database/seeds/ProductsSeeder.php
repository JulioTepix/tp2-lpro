<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProductsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('producto')->insert([
        'nombre' => "Bouteille d'eau 50cl",
        'marca' => 'Cristaline',
        'descripcion' => "Bouteille d'eau de 50 cl",
        'imagen' => 'cristaline-50cl.png',
        'stock' => 50,
        'precio' => 1.2
        ]);
    }
}
