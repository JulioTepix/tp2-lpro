<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
         $this->call(UserSeeder::class);
         $this->call(ProductsSeeder::class);
         $this->call(SuscriptionSeeder::class);
         $this->call(ClientSeeder::class);
         $this->call(SellSeeder::class);
         
         
    }
}
