<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SuscriptionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('suscripciones')->insert([
            'tipo_suscripcion' => 'Visit',
            'precio' => 2.5
        ]);
        DB::table('suscripciones')->insert([
            'tipo_suscripcion' => 'Hebdomadaire',
            'precio' => 6
        ]);
        DB::table('suscripciones')->insert([
            'tipo_suscripcion' => 'Bihebdomadaire',
            'precio' => 10
        ]);
        DB::table('suscripciones')->insert([
            'tipo_suscripcion' => 'Mensuel',
            'precio' => 15
        ]);
    }
}
