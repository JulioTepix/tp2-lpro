Cypress.on('uncaught:exception', (err, runnable) => { //uncaught excception ajouté pour permettre à l'application utiliser les input utilisé dans le projet
  return false
})

describe('login/logout', () => {
  beforeEach(() => cy.visit('http://127.0.0.1:8000'));

  it('login', () => {
    cy.login('juputepix5@gmail.com', '12345678');
    cy.logout('Julio')
  });
});

describe('gym - suscriptions', () => {
  beforeEach(() => 
  cy.login('juputepix5@gmail.com', '12345678'));

  it('add types of suscriptions', () => {
    cy.contains('Servicios').click()
    cy.contains('Suscripciones').click()
    cy.url().should('include', '/suscripciones/index')
    cy.get('[data-cy="new-sus"]').click()
    cy.url().should('include', '/suscripciones/create')
    cy.get('[data-cy="sus-type"]').type('Annuel')
    cy.get('[data-cy="sus-price"]').type('160')
    cy.get('[data-cy="submit"]').click()
    cy.contains('Annuel')
  });

  it('edit types of suscriptions', () => {
    cy.contains('Servicios').click()
    cy.contains('Suscripciones').click()
    cy.get('[data-cy="edit-sus"]').last().click()
    cy.url().should('include', '/update')
    cy.get('[data-cy="sus-price"]').clear().type('161')
    cy.get('[data-cy="submit"]').click()
    cy.contains('161')
  });

  it('delete types of suscriptions', () => {
    cy.contains('Servicios').click()
    cy.contains('Suscripciones').click()
    cy.get('[data-cy="delete-sus"]').last().click()
    cy.url().should('include', '/delete')
    cy.get('[data-cy="conf-delete"]').click()
  });

  afterEach(() => cy.logout('Julio'));
});

describe('gym - clients', () => {
  beforeEach(() => 
  cy.login('juputepix5@gmail.com', '12345678'));

  it('add client', () => {
    cy.contains('Clientes').click()
    cy.contains('Con suscripción').click()
    cy.get('[data-cy="new-client"]').click()
    cy.url().should('include', '/clientes/create')
    cy.get('[data-cy="client-name"]').type('Manuel')
    cy.get('[data-cy="client-lastn1"]').type('Perez')
    cy.get('[data-cy="client-lastn2"]').type('Rodriguez')
    cy.get('[data-cy="client-age"]').type('22')
    cy.get('[data-cy="client-tel"]').type('0721548659')
    cy.get('[data-cy="client-DPP"]').type('2023-01-15')
    cy.get('[data-cy="type-sus"]').select('Mensuel')
    cy.get('[data-cy="submit"]').click()
  });

  it('show data of the client', () => {
    cy.contains('Clientes').click()
    cy.contains('Con suscripción').click()
    cy.get('[data-cy="show-client"]').first().click()
    cy.url().should('include', '/show')
    cy.contains("Datos del cliente")
    cy.get('[data-cy="back"]').click()
  });

  it('edit client', () => {
    cy.contains('Clientes').click()
    cy.contains('Con suscripción').click()
    cy.get('[data-cy="edit-client"]').first().click()
    cy.url().should('include', '/update')
    cy.get('[data-cy="client-name"]').clear().type('Emmanuel')
    cy.get('[data-cy="client-lastn1"]').clear().type('Kan')
    cy.get('[data-cy="client-lastn2"]').clear().type('Ocasio')
    cy.get('[data-cy="client-age"]').clear().type('23')
    cy.get('[data-cy="client-tel"]').clear().type('0721548658')
    cy.get('[data-cy="client-DPP"]').type('2023-02-15')
    cy.get('[data-cy="type-sus"]').select('Mensuel')
    cy.get('[data-cy="submit"]').click()
  });

  it('search client', () => {
    cy.contains('Clientes').click()
    cy.contains('Con suscripción').click()
    cy.get('[data-cy="search-client"]').type('Mensuel')
    cy.get('[data-cy="search"]').click()
    cy.contains("Mensuel")
  });

  it('delete client', () => {
    cy.contains('Clientes').click()
    cy.contains('Con suscripción').click()
    cy.get('[data-cy="delete-client"]').first().click()
    cy.url().should('include', '/delete')
    cy.get('[data-cy="conf-delete"]').click() 
  });

  it('visualize visits', () => {
    cy.contains('Clientes').click()
    cy.contains('Visitas').click()
    cy.url().should('include', '/clientes/visitas')
  });
});

describe('gym - products', () => {
  beforeEach(() => 
  cy.login('juputepix5@gmail.com', '12345678'));

  it('add product', () => {
    cy.contains('Servicios').click()
    cy.contains('Productos').click()
    cy.url().should('include', '/productos/index')
    cy.get('[data-cy="new-prod"]').click()
    cy.url().should('include', '/productos/create')
    cy.get('[data-cy="prod-name"]').type("bouteille d'eau 1,5l")
    cy.get('[data-cy="prod-com"]').type('Cristaline')
    cy.get('[data-cy="prod-des"]').type("bouteille d'eau de 1,5l")
    cy.get('[data-cy="prod-stock"]').type('22')
    cy.get('[data-cy="prod-price"]').type('5')
    cy.get('input[type=file]').selectFile('src/img/cristaline-50cl.png')
    cy.get('[data-cy="submit"]').click()
  });

  it('edit product', () => {
    cy.contains('Servicios').click()
    cy.contains('Productos').click()
    cy.get('[data-cy="edit-prod"]').last().click()
    cy.url().should('include', '/update')
    cy.get('[data-cy="prod-name"]').clear().type("bouteille d'eau 2l")
    cy.get('[data-cy="prod-com"]').clear().type('Bonafont')
    cy.get('[data-cy="prod-des"]').clear().type("bouteille d'eau de 2l")
    cy.get('[data-cy="prod-stock"]').clear().type('10')
    cy.get('[data-cy="prod-price"]').clear().type('10')
    cy.get('[data-cy="submit"]').click()
  });

  it('search product', () => {
    cy.contains('Servicios').click()
    cy.contains('Productos').click()
    cy.get('[data-cy="search-prod"]').type('Cristaline')
    cy.get('[data-cy="search"]').click()
    cy.contains("Cristaline")
  });

  it('delete product', () => {
    cy.contains('Servicios').click()
    cy.contains('Productos').click()
    cy.get('[data-cy="delete-prod"]').last().click()
    cy.url().should('include', '/delete')
    cy.get('[data-cy="conf-delete"]').click() 
  });
});

describe('gym - sales', () => {
  beforeEach(() => 
  cy.login('juputepix5@gmail.com', '12345678'));

  it('sell product', () => {
    cy.contains('Ventas').click()
    cy.url().should('include', '/ventas/index')
    cy.get('[data-cy="new-sale"]').click()
    cy.url().should('include', '/ventas/create')
    cy.get('[data-cy="sale-prod"]').select("Bouteille d'eau 50cl")
    cy.get('[data-cy="sale-client"]').select('Otro(cliente externo)')
    cy.get('[data-cy="sale-cant"]').type('2')
    cy.get('[data-cy="submit"]').click()
    cy.url().should('include', '/ventas/store')
    cy.contains('Total')
  });

  it('search sale', () => {
    cy.contains('Ventas').click()
    cy.get('[data-cy="search-sale"]').type('Julio')
    cy.get('[data-cy="search"]').click()
    cy.contains("Julio")
  }); 

  it('delete sale', () => {
    cy.contains('Ventas').click()
    cy.get('[data-cy="delete-sale"]').first().click()
    cy.url().should('include', '/delete')
    cy.get('[data-cy="conf-delete"]').click() 
  });

});

describe('gym - profits', () => {
  beforeEach(() => 
  cy.login('juputepix5@gmail.com', '12345678'));

  it('visualize profits of today', () => {
    cy.contains('Ganancias').click()
    cy.contains('Ganancias de hoy')
    cy.url().should('include', '/ganancias/index')
  });

  it('search profits of a different day', () => {
    cy.contains('Ganancias').click()
    cy.get('[data-cy="search-profit"]').type('2022-12-13')
    cy.get('[data-cy="search"]').click()
  });

});