/*CREATE DATABASE goldensfitness;*/


CREATE TABLE suscripciones(
        id_suscripcion int AUTO_INCREMENT PRIMARY KEY,
        tipo_suscripcion varchar(30) NOT NULL,
        precio float NOT NULL
);

CREATE TABLE cliente(
        id_cliente int AUTO_INCREMENT PRIMARY KEY,
        nombre varchar(30) NOT NULL,
        apellido_paterno varchar(30) NOT NULL,
        apellido_materno varchar(30) NOT NULL,
        edad int NOT NULL,
        telefono varchar(30),
        fecha_inscripcion date DEFAULT CURRENT_TIMESTAMP,
        fecha_pago date NULL,
        fecha_proximo_pago date NULL,
        id_suscripcion int NOT NULL,
        estado boolean DEFAULT True,
        FOREIGN KEY(id_suscripcion) REFERENCES suscripciones(id_suscripcion) ON UPDATE CASCADE ON DELETE CASCADE
);


CREATE TABLE producto(
        id_producto int AUTO_INCREMENT PRIMARY KEY,
        nombre varchar(30) NOT NULL,
        marca varchar(30) NOT NULL,
        descripcion varchar(100) NOT NULL,
        imagen varchar(60)NOT NULL,
        stock int NOT NULL,
        precio float NOT NULL,
        estado boolean DEFAULT True
);

CREATE TABLE ventas(
        id_ventas int AUTO_INCREMENT PRIMARY KEY,
        id_producto int NOT NULL,
        id_cliente int,
        cantidad varchar(20) NOT NULL,
        total_pago float NOT NULL,
        create_by varchar(30),
        fecha date DEFAULT CURRENT_TIMESTAMP,
        FOREIGN KEY(id_producto) REFERENCES producto(id_producto) ON UPDATE CASCADE ON DELETE CASCADE,
        FOREIGN KEY(id_cliente) REFERENCES cliente(id_cliente) ON UPDATE CASCADE ON DELETE CASCADE
);



CREATE OR REPLACE VIEW venta_completa AS 
SELECT cl.id_cliente, v.id_ventas, pro.id_producto, CONCAT(cl.nombre, ' ', cl.apellido_paterno, ' ',cl.apellido_materno) AS nombre, pro.nombre AS producto, pro.precio, v.cantidad, v.total_pago, pro.stock, v.create_by, v.fecha
FROM  cliente cl
     RIGHT JOIN ventas v ON cl.id_cliente=v.id_cliente
     INNER JOIN producto pro ON v.id_producto=pro.id_producto;
     
CREATE OR REPLACE VIEW nombre_clientes AS 
SELECT id_cliente, CONCAT(nombre, ' ', apellido_paterno, ' ', apellido_materno) AS nombre
FROM  cliente
WHERE estado=1;

CREATE OR REPLACE VIEW clientes_sus AS 
SELECT c.id_cliente, CONCAT(c.nombre, ' ', c.apellido_paterno, ' ', c.apellido_materno) AS nombre, c.fecha_pago, c.edad, c.telefono, c.fecha_inscripcion, c.fecha_proximo_pago, c.id_suscripcion, c.estado, ss.tipo_suscripcion, ss.precio
FROM  cliente c
        INNER JOIN suscripciones ss ON c.id_suscripcion=ss.id_suscripcion;

