# Tp2 Lpro TEPIXTLE Julio



## Getting started

Premièrement il faut savoir que le projet utilisé pour les test est fait avec le framework [Laravel 7](https://laravel.com/docs/7.x)

## Installation
Pour que toute marche bien, après cloner le projet il faut installer composer dans le répertoire packages/gym avec

```bash
 composer install
```

## Pre - requis

 - Pour ce projet il est nécessaire d'avoir xampp avec PHP >= 7.2.5
 Il y a une commande pré défini pour les utilisateurs linux:


```
 npx nx run gym:phplinux
```
 - Ce projet utilise MySQL et pour raisons de comment a été développé, il faut créer la base de données manuellement et après copier et coller le script du fichier gym.sql

## Procedure des commandes

1. Clear
```
 npx nx run gym:clear
```
2. Verifier la conexion avec MySQL (Modifier le .env si besoin)
```
 npx nx run gym:migrate
```
3. Commande pour les seeders
```
 npx nx run gym:seeding
```
4. Executer le server
```
 npx nx run gym:serve
```
5. Faire les tests